import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { Router, browserHistory } from 'react-router';

// Imports
import './assets/styles/index.styl';
import reducers from './reducers/rootReducer';
import routes from './routes';

// Redux
const store = applyMiddleware(thunkMiddleware)(createStore)(reducers);

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>,
    document.getElementById('root')
);