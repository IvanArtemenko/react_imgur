import React from 'react';
import cookie from 'react-cookie';
import { connect } from 'react-redux';

// Components
import Header from './components/Header';

// Actions
import { logout } from './actions/users';

class App extends React.Component {
    render() {
        return (
            <div className="app">
                <Header userName={cookie.load('user_name')}
                        logout={!!cookie.load('user_name') ? this.props.logout : false} />

                {this.props.children}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state
    }
}

export default connect(mapStateToProps, {
    logout
})(App);
