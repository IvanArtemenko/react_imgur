import axios from 'axios';
import cookie from 'react-cookie';
import {
    LOAD_ALBUMS,
    LOAD_ALBUMS_ERROR,
    LOAD_ALBUM_IMAGES
} from '../constants/actionTypes';

const options = {
    'headers': {
        'Authorization': `Bearer ${cookie.load('access_token')}`
    }
};

export const loadAlbums = () => dispatch => {
    axios.get(`https://api.imgur.com/3/account/${cookie.load('user_name')}/albums`, options)
        .then(response => dispatch({
            type: LOAD_ALBUMS,
            payload: response.data.data
        }))
        .catch(error => dispatch({
            type: LOAD_ALBUMS_ERROR,
            payload: error.message
        }));
};

export const loadAlbumImages = (id) => dispatch => {
    axios.get(`https://api.imgur.com/3/album/${id}`, options)
        .then(response => dispatch({
            type: LOAD_ALBUM_IMAGES,
            payload: response.data.data
        }));
};
