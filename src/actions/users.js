import cookie from 'react-cookie';

import {
    USER_LOGIN_SUCCESS,
    USER_LOGOUT
} from '../constants/api';


export const login = () => {
  return {
      type: USER_LOGIN_SUCCESS,
      payload: true
  }
};

export const logout = () => dispatch => {
    cookie.remove('access_token');
    cookie.remove('user_name');
    cookie.remove('refresh_token');

    return dispatch({
        type: USER_LOGOUT,
        payload: true
    });
};