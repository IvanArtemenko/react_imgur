import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './App';
import Main from './containers/Main';
import Login from './containers/Login';
import AlbumPage from './containers/AlbumPage';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Main} />
        <Route path="/login" component={Login} />
        <Route path="/album/:id" component={AlbumPage} />
    </Route>
);