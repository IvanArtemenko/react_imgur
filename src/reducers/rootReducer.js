import { combineReducers } from 'redux';
import usersReducer from './usersReducer';
import albumsReducer from './albumsReducer';

const rootReducer = combineReducers({
    user: usersReducer,
    data: albumsReducer
});

export default rootReducer;