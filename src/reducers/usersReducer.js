import {
    USER_LOGOUT,
    USER_LOGIN_SUCCESS
} from '../constants/api';

const initialState = {
    loggedIn: false,
    loggedOut: false
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGIN_SUCCESS:
            return {...state, loggedIn: action.payload};
        case USER_LOGOUT:
            return {...state, loggedOut: action.payload};
        default:
            return state;
    }
};

export default usersReducer;