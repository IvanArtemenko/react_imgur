import {
    LOAD_ALBUMS,
    LOAD_ALBUMS_ERROR,
    LOAD_ALBUM_IMAGES
} from '../constants/actionTypes';

const initialState = {
    albums: [],
    albumImages: []
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_ALBUMS:
            return {...state, albums: action.payload};
        case LOAD_ALBUMS_ERROR:
            return {...state, albums: action.payload};
        case LOAD_ALBUM_IMAGES:
            return {...state, albumImages: action.payload};
        default:
            return state;
    }
};

export default albumsReducer;