export const LOAD_ALBUMS = 'LOAD_ALBUMS';
export const LOAD_ALBUMS_ERROR = 'LOAD_ALBUMS_ERROR';
export const LOAD_ALBUM_IMAGES = 'LOAD_ALBUM_IMAGES';