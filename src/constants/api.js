export const CLIENT_ID = '7e65a32866eebbe';
export const AUTHORIZATION_URL = 'https://api.imgur.com/oauth2/authorize';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';