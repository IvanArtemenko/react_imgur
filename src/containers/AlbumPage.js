import React from 'react';
import { connect } from 'react-redux';
import { loadAlbumImages } from '../actions/albums';

class AlbumPage extends React.Component {
    componentDidMount() {
        this.props.loadAlbumImages(this.props.params.id);
    }

    render() {
        return (
            <div className="album">
                <div className="container">
                    <div className="row-fluid">
                        <header className="album-title">
                            <h3>{this.props.data.albumImages.title}</h3>
                            <p>{this.props.data.albumImages.description}</p>
                        </header>
                    </div>
                    <div className="row">
                        {this.props.data.albumImages.images ?
                            this.props.data.albumImages.images.map((item, index) => {
                                return (
                                    <div className="col-md-3" key={index}>
                                        <div className="image-item">
                                            <a href={item.link}><img className="img-responsive" src={item.link} alt="img"/></a>
                                        </div>
                                    </div>
                                )

                            }) : <p>Loading...</p>}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        data: state.data
    }
}

export default connect(mapStateToProps, { loadAlbumImages })(AlbumPage);