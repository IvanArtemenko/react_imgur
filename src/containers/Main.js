import React from 'react';
import { connect } from 'react-redux';
import cookie from 'react-cookie';

// Components
import LoginWindow from '../components/LoginWindow';
import AlbumWrapper from '../components/AlbumWrapper';

// Actions
import { loadAlbums } from '../actions/albums';
import { logout } from '../actions/users';

class Main extends React.Component {
    componentDidMount() {
        this.props.loadAlbums();
    }

    render() {
        console.log(cookie.load('user_name'), this.props.data);
        return (
            <div>
                <div className="container">
                    <header>
                        <h3>Albums</h3>
                    </header>
                    {!cookie.load('user_name') ?
                        <LoginWindow /> :
                        <AlbumWrapper items={this.props.data} />
                    }
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        data: state.data,
        user: state.user
    }
}


export default connect(mapStateToProps, { loadAlbums, logout })(Main);