import React from 'react';
import cookie from 'react-cookie';

class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            loggedIn: false
        }
    }

    componentDidMount() {
        let token = window.location.hash.match(/access_token=(\w+)/);
        let userName = window.location.hash.match(/account_username=(\w+)/);
        let refreshToken = window.location.hash.match(/refresh_token=(\w+)/);
        cookie.save('access_token', token[1]);
        cookie.save('user_name', userName[1]);
        cookie.save('refresh_token', refreshToken[1]);

        this.setState({loggedIn: true});
    }

    render() {

        if (this.state.loggedIn) {
            return (
                <div className="container">
                    <div className="alert alert-success">
                        <strong>Welcome</strong> {cookie.load('user_name')}
                        {window.location.href = '/'}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="container">
                    <div className="alert alert-danger">
                        <strong>Sorry</strong> access denied
                        {window.location.href = '/'}
                    </div>
                </div>
            );
        }
    }
}

export default Login;