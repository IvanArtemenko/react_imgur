import React from 'react';
import {
    AUTHORIZATION_URL,
    CLIENT_ID
} from '../constants/api';

const LoginWindow = props =>
    <div className="login-window">
        <header className="login-window__header">
            <h2>Hello you're not logged in, press login and allow us to load your data</h2>
        </header>

        <a  className="btn btn-primary"
            href={`${AUTHORIZATION_URL}?client_id=${CLIENT_ID}&response_type=token&state=APPLICATION_STATE&redirect_uri=http://localhost:3000/login`}>Login</a>
    </div>;

export default LoginWindow;