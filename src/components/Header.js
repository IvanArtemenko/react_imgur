import React from 'react';
import { Link } from 'react-router';

const Header = props =>
    <header className="navbar navbar-default">
        <div className="container">
            <div className="navbar-header">
                <Link className="navbar-brand" to="/">React Imgur App</Link>
            </div>
            <ul className="nav navbar-nav navbar-right">
                <li>
                    <Link to="/">{props.userName}</Link>
                </li>
                <li>
                    {props.logout ? <button className="btn btn-primary btn-sm" onClick={props.logout}>Logout</button> : ''}
                </li>
            </ul>
        </div>
    </header>;

export default Header;