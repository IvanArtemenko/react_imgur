import React from 'react';
import AlbumItem from './AlbumItem';

class AlbumWrapper extends React.Component {
    render() {
        return (
            <div className="album-wrapper">
                <div className="row">
                    {this.props.items.albums.map((item, index) => {
                        return <AlbumItem
                            itemsCount={item.images_count}
                            title={item.title}
                            id={item.id}
                            coverImage={`http://i.imgur.com/${item.cover}.png`}
                            key={index} />;
                    })}
                </div>
            </div>
        );
    }
}

export default AlbumWrapper;