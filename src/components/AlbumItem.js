import React from 'react';
import { Link } from 'react-router';

const AlbumItem = props =>
    <div className="col-md-3">
        <div className="album-item">
            <div className="album-image">
                <img src={props.coverImage} className="img-responsive" alt="cover" />
            </div>
            <div className="album-description">
                <Link to={`/album/${props.id}`} className="album-title">{props.title}</Link>
                <div className="album-meta">
                    <span>Images: {props.itemsCount}</span>
                </div>
            </div>
        </div>
    </div>;

export default AlbumItem;
